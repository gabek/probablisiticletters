from letter_area import area_of_letter
from probabilistic_network import calculate_z

#Gabe Kelly
#11/26/18

def identify(directory, alpha, nums):
    dic = area_of_letter(directory)
    letter = ''
    max_v = 0
    for key in list(dic.keys()):
        values = dic.get(key)
        total = calculate_z(nums[0], nums[1], alpha, values)
        if total > max_v:
            letter = key
            max_v = total
    return letter


test_array = [[0.58,  0.42], [.40, .29], [.44,.42],[.49,.42],[.54,.43]]
expected = ["y","x","b","t","o"]
#Feel free to change these values to match values of the letters and array

for i in range(0,len(test_array)):
    letter = identify("../letters", .5, test_array[i])
    print(letter)
    if letter == expected[i]:
        print("Letter Properly Identified")
    else:
        print("Incorrectly identified")
