import numpy as np

#Gabe Kelly
#11/26/18

def calculate_z(x, y, alpha, point_array):
    max_z = 0
    for a in point_array:
        z = formula(x, y, alpha, a[0], a[1])
        if z > max_z:
            max_z = z
    return max_z

def formula(x,y,alpha,x_o, y_o):
    top = -1*((((x-x_o)**2))+((y-y_o)**2))
    return 2.71828**(top/(2*(alpha**2)))
