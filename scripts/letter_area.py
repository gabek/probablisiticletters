from read_letters import read_files
import numpy as np

#Gabe Kelly
#11/26/18

def area(letter):
    width = 0.0
    height = 0.0
    for row in letter:
        start = 0
        end = 0
        for i in range(0, len(row)):
            if row[i] == 1:
                if start == 0:
                    start = i
                end = i
        wid = (float(end-start))/59.0
        if wid>width:
            width=wid
    for col in letter.T:
        start = 0
        end = 0
        for i in range(0, len(col)):
            if col[i] == 1:
                if start == 0:
                    start = i
                end = i
        hig = (float(end-start))/76.0
        if hig>height:
            height=hig
    return np.array([width, height])

def get_letter(letter):
    l = letter.replace(".png", "")
    l = l.replace("Upper","")
    l = l.replace("Lower","")
    return l


def area_of_letter(directory):
    dictionary = read_files(directory)
    out_dic = {}
    for l in list(dictionary.keys()):
        a = area(dictionary[l])
        let = get_letter(l)
        if (out_dic.get(let) is None) == False:
            t = np.asarray(out_dic.get(let))
            array = np.array([t,a])
            out_dic[let] = tuple(array)
        else:
            out_dic[let] = tuple(a) 
    return out_dic
