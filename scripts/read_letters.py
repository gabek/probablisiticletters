import numpy as np
import scipy as sp
from PIL import Image, ImageOps
import os

#Gabe Kelly
#11/26/18

def read_files(directory):
   listdir = os.listdir(directory)
   out_dictionary = {}
   for f in listdir:
       image = Image.open(directory + "/" + f).convert('L')
       img_inv = ImageOps.invert(image)
       letter = np.array(img_inv)
       letter[letter >0] = 1
       out_dictionary[f] = letter
   return out_dictionary
